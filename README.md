## Getting started

1. cd `server`, `npm install`, run with `node index.js`; server will be running on port `9595`
2. cd `client`, `npm install`, run with `npm start`; client will be running on port `3000`
3. Open browser to localhost:3000 to view app
