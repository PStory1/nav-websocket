import * as React from 'react';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepButton from '@mui/material/StepButton';
import Typography from '@mui/material/Typography';

const socket = new WebSocket('ws://localhost:8082');

export default function HorizontalNonLinearStepper() {
  const [activeStep, setActiveStep] = React.useState(0);
  const [completed, setCompleted] = React.useState({});
  const ws = React.useRef(socket);

  React.useEffect(() => {
    fetch('http://localhost:9595/cart')
      .then((response) => response.json())
      .then((completedSteps) => {
        handleComplete(completedSteps);
      });
  }, []);

  // listen for messages from web socket (coming from AddToCart endpoint, and updating which steps are completed)
  React.useEffect(() => {
    ws.current?.addEventListener('message', ({ data }) => {
      const completedSteps = JSON.parse(data);
      handleComplete(completedSteps);
    });
  }, []);

  const steps = ['Catalog', 'Summary', 'Review'];

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const handleComplete = (completedSteps) => {
    const newCompleted = completed;
    for (let step of completedSteps) {
      newCompleted[step] = true;
    }
    setCompleted(newCompleted);
  };

  return (
    <Box sx={{ width: '100%' }}>
      <Stepper nonLinear activeStep={activeStep}>
        {steps.map((label, index) => (
          <Step key={label} completed={completed[label]}>
            <StepButton color="inherit" onClick={handleStep(index)}>
              {label}
            </StepButton>
          </Step>
        ))}
      </Stepper>
      <div>
        <React.Fragment>
          <Typography sx={{ mt: 2, mb: 1 }}>Step {activeStep + 1}</Typography>
        </React.Fragment>
      </div>
    </Box>
  );
}
