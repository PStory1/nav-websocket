import './App.css';
import Stepper from './Stepper.js';
import CompletionButtons from './CompletionButtons.js';



function App() {
  return (
    <div className="App">
          <Stepper />
          <CompletionButtons />
    </div>
  );
}

export default App;
