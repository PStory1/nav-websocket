import * as React from 'react';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';

export default function BasicButtonGroup() {
  let settings = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };

  // the following functions are simply pushing data to cart, this component doesn't need to know about web sockets,
  // only the cart service

  const completeCatalog = async () => {
    settings['body'] = JSON.stringify({ device: 'asdf' });
    try {
      const res = await fetch('http://localhost:9595/cart', settings);
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  };

  const completeSummary = async () => {
    settings['body'] = JSON.stringify({
      'trade-in': 'asdf',
      protection: 'asdf',
      pricing: 'asdf',
    });
    try {
      const res = await fetch('http://localhost:9595/cart', settings);
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  };
  const completeReview = async () => {
    settings['body'] = JSON.stringify({ review: 'asdf' });
    try {
      const res = await fetch('http://localhost:9595/cart', settings);
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <ButtonGroup variant="contained" aria-label="outlined primary button group">
      <Button onClick={() => completeCatalog()}>Complete catalog</Button>
      <Button onClick={() => completeSummary()}>Complete summary</Button>
      <Button onClick={() => completeReview()}>Complete review</Button>
    </ButtonGroup>
  );
}
