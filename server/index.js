const WebSocket = require('ws');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
app.use(bodyParser.json());
app.use(cors());
const port = 9595;

const wss = new WebSocket.Server({ port: 8082 });
wss.on('connection', (ws) => {
  console.log('New client connected!');

  ws.on('close', () => {
    console.log('Client has disconnected');
  });
});

// abstract out the mapping, checkCompletion, etc. into nav-stepper client
// instead of broker endpoint checking / sending completion, send the entire cart each time it's updated,
// and let nav-stepper check completion

// each step (catalog, summary, review) has an array of keys that must be present in the cart for the step to be considered complete
// if cart contains 'device', 'catalog' step is complete
// if cart contains 'trade-in', 'protection', and 'pricing', 'summary' step is complete
// if cart contains 'review', 'review' step is complete
const completionMapping = {
  Catalog: ['device'],
  Summary: ['trade-in', 'protection', 'pricing'],
  Review: ['review'],
};

// assume cart starts as empty object and objects such as 'device', 'trade-in', etc. are added to it
let cart = {};

// check that the cart contains the completion criteria of a particular step
const checkCompletion = (cart, step) => {
  return step.every((s) => cart.hasOwnProperty(s));
};

// iterate through completion mapping and return array of completed steps
const completedSteps = () => {
  const steps = [];

  for (let step in completionMapping) {
    if (checkCompletion(cart, completionMapping[step])) {
      steps.push(step);
    }
  }

  return steps;
};

// used when stepper is initially loaded to inform which steps are completed
app.get('/cart', (req, res) => {
  res.send(JSON.stringify(completedSteps()));
});

// each time an object is added to the cart, check which steps are completed and push as a message to each connected client via websocket
app.post('/cart', (req, res) => {
  cart = { ...cart, ...req.body };
  wss.clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify(completedSteps()));
    }
  });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
